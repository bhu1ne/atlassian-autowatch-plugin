package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.service.email.EmailService;
import com.atlassian.addon.connect.autowatch.util.JiraClient;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.http.HttpStatus;
import play.Logger;
import play.libs.F;
import play.libs.WS;

public class IssueWatchServiceImpl implements IssueWatchService
{
    private final EmailService emailService;
    private final JiraClient client;

    @Inject
    public IssueWatchServiceImpl(EmailService emailService, JiraClient client)
    {
        this.emailService = emailService;
        this.client = client;
    }

    @Override
    public void watch(final AutoWatchRule autoWatchRule, final Issue issue, final AcHost acHost)
    {
        String watchUri = String.format("/rest/api/2/issue/%s/watchers", issue.getKey());
        final WS.Response currentWatchers = client.url(watchUri, acHost, Option.some(autoWatchRule.ruleOwner.key)).get().get();

        WS.WSRequestHolder url = client.url(watchUri, acHost, Option.some(autoWatchRule.ruleOwner.key));
        for (final JiraUser user : watchersToAdd(currentWatchers, autoWatchRule))
        {
            url.post(new TextNode(user.key)).map(new F.Function<WS.Response, Void>()
            {
                @Override
                public Void apply(WS.Response response) throws Throwable
                {
                    if (response.getStatus() == HttpStatus.SC_NO_CONTENT)
                    {
                        Logger.info("Successfully added user as a watcher of issue " + issue.getId());
                        // TODO: send emails after finding an email service that works (/rest/atlassian-connect/1/email is not white-listed in the Connect scopes)
                        // emailService.sendEmail(user, acHost, issue);
                    }
                    return null;
                }
            });
        }
    }

    private Iterable<JiraUser> watchersToAdd(final WS.Response currentWatchers, final AutoWatchRule autoWatchRule)
    {
        final JsonNode watchers = currentWatchers.asJson().get("watchers");
        final ImmutableList.Builder<String> userKeyBuilder = ImmutableList.builder();
        for (int i = 0; i < watchers.size(); i++)
        {
            final String userKey = watchers.get(i).get("key").asText();
            userKeyBuilder.add(userKey);
        }
        final ImmutableList<String> userKeys = userKeyBuilder.build();
        return Iterables.filter(autoWatchRule.users, new Predicate<JiraUser>()
        {
            @Override
            public boolean apply(final JiraUser jiraUser)
            {
                return !userKeys.contains(jiraUser.key);
            }
        });
    }
}
