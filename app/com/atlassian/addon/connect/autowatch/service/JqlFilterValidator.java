package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.fugue.Option;

public interface JqlFilterValidator
{
    Option<ValidationErrors.ValidationError> validate(AutoWatchRule autoWatchRule);
}
