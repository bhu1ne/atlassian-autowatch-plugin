package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface UserService
{
    List<JiraUser> getUsers(JsonNode jsonNode, String tenant);

    JiraUser getUser(String userKey, String tenant);
}
