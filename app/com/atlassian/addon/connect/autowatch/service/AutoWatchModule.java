package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.service.email.EmailService;
import com.atlassian.addon.connect.autowatch.service.email.EmailServiceImpl;
import com.atlassian.addon.connect.autowatch.util.JiraClient;
import com.atlassian.addon.connect.autowatch.util.JiraClientImpl;
import com.atlassian.addon.connect.autowatch.util.permission.PermissionService;
import com.atlassian.addon.connect.autowatch.util.permission.PermissionServiceImpl;
import com.google.inject.AbstractModule;

public class AutoWatchModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(AutoWatchRuleService.class).to(AutoWatchRuleServiceImpl.class);
        bind(SearchService.class).to(SearchServiceImpl.class);
        bind(IssueWatchService.class).to(IssueWatchServiceImpl.class);
        bind(UserService.class).to(UserServiceImpl.class);
        bind(IssueEventHandler.class).to(IssueEventHandlerImpl.class);
        bind(EmailService.class).to(EmailServiceImpl.class);
        bind(NewIssuesScannerService.class).to(NewIssuesScannerServiceImpl.class);
        bind(JqlFilterValidator.class).to(JqlFilterValidatorImpl.class);
        bind(UserValidator.class).to(UserValidatorImpl.class);
        bind(JiraClient.class).to(JiraClientImpl.class);
        bind(PermissionService.class).to(PermissionServiceImpl.class);
    }
}
