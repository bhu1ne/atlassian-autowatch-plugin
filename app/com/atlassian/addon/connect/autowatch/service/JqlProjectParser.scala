package com.atlassian.addon.connect.autowatch.service

import util.parsing.combinator.RegexParsers

class JqlProjectParser extends RegexParsers
{

  val PROJECT_KEY_OR_NAME = """[a-zA-Z]([a-zA-Z]\s*|_[a-zA-Z]\s*)*""" r

  def projectQuery = "project" ~ operator ~ projectLValue ^^^ true

  def operator: Parser[String] = "in" | "=" | "!=" | "is not" | "is" | "not in"

  def projectLValue: Parser[Boolean] = opt("(") ~ projects ~ opt(")") ^^^ true

  def projects: Parser[Boolean] = rep1sep(projectKey, ",") ^^^ true

  def projectKey: Parser[Boolean] = opt("\"") ~ PROJECT_KEY_OR_NAME ~ opt("\"") ^^^ true

  def logicalOp: Parser[String] = "and" | "or"

  def parse(queryString: String): Boolean = parse(projectQuery, queryString.toLowerCase) match
  {
    case Success(result, _) => result
    case failure: NoSuccess => false
  }

}
