package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.fugue.Option;

import java.util.List;

public interface UserValidator
{

    Option<ValidationErrors.ValidationError> validate(List<JiraUser> users);

}
