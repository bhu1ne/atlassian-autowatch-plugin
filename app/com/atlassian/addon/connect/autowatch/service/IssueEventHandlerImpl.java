package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.AcHost;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;

import java.util.List;

public class IssueEventHandlerImpl implements IssueEventHandler
{
    private final SearchService searchService;
    private final IssueWatchService issueWatchService;

    @Inject
    public IssueEventHandlerImpl(SearchService searchService, IssueWatchService issueWatchService)
    {
        this.searchService = searchService;
        this.issueWatchService = issueWatchService;
    }

    @Override
    public void handle(final Issue issue)
    {
        final AcHost acHost = AC.getAcHost();

        final List<AutoWatchRule> allTenantRules = AutoWatchRule.getAllRules(acHost.getKey());

        // sadly, this is how we check whether issue matches JQL of AutoWatch rule.
        final Iterable<AutoWatchRule> filter = Iterables.filter(allTenantRules, new Predicate<AutoWatchRule>()
        {
            @Override
            public boolean apply(final AutoWatchRule autoWatchRule)
            {
                return searchService.find(autoWatchRule, issue.getKey(), acHost);
            }
        });

        for (final AutoWatchRule rule : filter)
        {
            issueWatchService.watch(rule, issue, acHost);
        }
    }
}
