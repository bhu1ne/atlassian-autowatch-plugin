package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.connect.play.java.AC;
import com.atlassian.fugue.Option;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class JqlFilterValidatorImpl implements JqlFilterValidator
{
    private final SearchService searchService;
    private final JqlProjectParser jqlProjectParser;

    @Inject
    public JqlFilterValidatorImpl(SearchService searchService)
    {
        this.searchService = searchService;
        this.jqlProjectParser = new JqlProjectParser();
    }

    @Override
    public Option<ValidationErrors.ValidationError> validate(AutoWatchRule autoWatchRule)
    {
        if (StringUtils.isEmpty(autoWatchRule.filter))
        {
            return Option.some(new ValidationErrors.ValidationError("filter", "JQL filter can't be empty."));
        }
        else if (!searchService.validateJql(AC.getAcHost(autoWatchRule.tenant).get(), autoWatchRule.project, autoWatchRule.filter, autoWatchRule.ruleOwner))
        {
            return Option.some(new ValidationErrors.ValidationError("filter", "Invalid JQL filter"));
        }
        else if (jqlProjectParser.parse(autoWatchRule.filter))
        {
            return Option.some(new ValidationErrors.ValidationError("filter", "You can't execute project queries here"));
        }
        else
        {
            return Option.none();
        }
    }
}
