package com.atlassian.addon.connect.autowatch.service.email;

import com.atlassian.addon.connect.autowatch.model.Issue;
import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.AcHost;

public class EmailContext
{
    private final Issue issue;
    private final AcHost tenant;

    public EmailContext(Issue issue)
    {
        this.issue = issue;
        this.tenant = AC.getAcHost();
    }

    public Issue getIssue()
    {
        return issue;
    }

    public AcHost getTenant()
    {
        return tenant;
    }

    public String getIssueUrl()
    {
        return this.getTenant().getBaseUrl() + "/browse/" + this.getIssue().getKey();
    }
}
