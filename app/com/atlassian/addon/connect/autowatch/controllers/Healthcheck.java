package com.atlassian.addon.connect.autowatch.controllers;

import com.atlassian.addon.connect.autowatch.util.VersionUtils;
import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.model.AcHostModel;
import com.google.common.collect.ImmutableMap;
import play.Logger;
import play.db.jpa.JPA;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.lang.management.ManagementFactory;
import java.util.Map;

import static java.net.HttpURLConnection.HTTP_UNAVAILABLE;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

public class Healthcheck extends Controller
{

    @play.db.jpa.Transactional
    public Result index() {

        try{
            return ok(Json.toJson(
                    ImmutableMap.builder()
                            .putAll(basicHealthInfo())
                            .put("jpa", getJPAStats())
                            .put("isHealthy", true)
                            .build()
            ));
        }

        catch(Throwable e)
        {
            Logger.error("Health check failed to access jpa", e);
            return status(HTTP_UNAVAILABLE, Json.toJson(
                            ImmutableMap.builder()
                                .putAll(basicHealthInfo())
                                .put("isHealthy", false)
                                .put("failureReason", "Health check failed message:" + e.getMessage())
                                .build()
            ));
        }
    }

    private Map<String,Object> getJPAStats() throws Throwable {
        return JPA.withTransaction(new F.Function0<Map<String, Object>>()
        {
            @Override
            public Map<String, Object> apply() throws Throwable
            {
                final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
                builder.put("rulesCount", JPA.em().createNamedQuery("AutoWatchRule.count", Long.class).getSingleResult());
                return builder.build();
            }
        });

    }

    private Map<String, Object> basicHealthInfo() {
        return ImmutableMap.<String, Object>builder()
                .put("name", AC.PLUGIN_NAME)
                .put("key", AC.PLUGIN_KEY)
                .put("version", VersionUtils.VERSION)
                .put("dyno", defaultIfEmpty(System.getenv().get("DYNO"), "unknown"))
                .put("hosts", AcHostModel.all().size())
                .put("time", System.currentTimeMillis())
                .put("freeMemory", Runtime.getRuntime().freeMemory())
                .put("systemLoad", ManagementFactory.getOperatingSystemMXBean().getSystemLoadAverage())
                .build();
    }



}


