package com.atlassian.addon.connect.autowatch.controllers.form;

import play.data.validation.Constraints;

public class ProjectAutoWatchRuleForm
{
    @Constraints.Required
    public String projectKey;
    public String tenant;
    public String tenantHost;

    public ProjectAutoWatchRuleForm(String projectKey, String tenant, String tenantHost)
    {
        this.projectKey = projectKey;
        this.tenant = tenant;
        this.tenantHost = tenantHost;
    }
}
