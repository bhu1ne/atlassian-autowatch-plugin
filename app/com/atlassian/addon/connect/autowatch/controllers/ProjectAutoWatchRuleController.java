package com.atlassian.addon.connect.autowatch.controllers;

import com.atlassian.addon.connect.autowatch.controllers.form.ProjectAutoWatchRuleForm;
import com.atlassian.addon.connect.autowatch.util.permission.HasAnyPermission;
import com.atlassian.addon.connect.autowatch.util.permission.Permission;
import com.atlassian.addon.connect.autowatch.util.permission.PermissionService;
import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.connect.play.java.auth.jwt.AuthenticateJwtRequest;
import com.google.common.collect.ImmutableList;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.net.URI;

public class ProjectAutoWatchRuleController extends Controller
{
    private final PermissionService permissionService;

    @Inject
    public ProjectAutoWatchRuleController(PermissionService permissionService)
    {
        this.permissionService = permissionService;
    }

    @AuthenticateJwtRequest
    @HasAnyPermission (permissions = {
            Permission.PROJECT_ADMIN,
            Permission.ADMINISTER,
            Permission.SYSTEM_ADMIN
    })
    public Result addAutoWatchRule(final String projectKey)
    {
        final String userId = AC.getUser().getOrNull();
        final AcHost host = AC.getAcHost();

        final boolean hasPermission = permissionService.hasAnyPermission(userId, ImmutableList.of(Permission.MANAGE_WATCHER_LIST), projectKey, host);
        if (!hasPermission)
        {
            return ok(views.html.permissionneeded.render(createTemplateParamsWithoutPremission(projectKey)));
        }
        else
        {
//            Logger.info("Project id is " + projectKey + " tenant is " + AC.getAcHost().getKey());

            return ok(views.html.configureproject.render(createTemplateParamsWithPremission(projectKey)));
        }
    }

    private static String getBaseUrl()
    {
        final URI uri = URI.create(AC.getAcHost().getBaseUrl());
        final String port;
        if (uri.getPort() > 0)
        {
            port = ":" + uri.getPort();
        }
        else
        {
            port = "";
        }
        return "//" + uri.getHost() + port;
    }

    private static Form<ProjectAutoWatchRuleForm> createTemplateParamsWithoutPremission(String projectKey)
    {
        return Form.form(ProjectAutoWatchRuleForm.class)
                .fill(new ProjectAutoWatchRuleForm(projectKey, AC.getAcHost().getKey(), AC.getAcHost().getBaseUrl()));
    }

    private static Form<ProjectAutoWatchRuleForm> createTemplateParamsWithPremission(String projectKey)
    {
        return Form.form(ProjectAutoWatchRuleForm.class)
                .fill(new ProjectAutoWatchRuleForm(projectKey, AC.getAcHost().getKey(), getBaseUrl()));
    }
}
