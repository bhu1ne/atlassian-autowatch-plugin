package com.atlassian.addon.connect.autowatch.controllers;

import com.atlassian.addon.connect.autowatch.model.AutoWatchRule;
import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.service.AutoWatchRuleService;
import com.atlassian.addon.connect.autowatch.service.UserService;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.addon.connect.autowatch.util.permission.HasAnyPermission;
import com.atlassian.addon.connect.autowatch.util.permission.Permission;
import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.token.CheckValidToken;
import com.atlassian.connect.play.java.token.Token;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

import java.util.List;

/**
 * REST resource for Project AutoWatch Rules.
 */
public class ProjectAutoWatchRuleResource extends Controller
{
    private final AutoWatchRuleService autoWatchRuleService;
    private final UserService userService;

    @Inject
    public ProjectAutoWatchRuleResource(AutoWatchRuleService autoWatchRuleService, UserService userService)
    {
        this.autoWatchRuleService = autoWatchRuleService;
        this.userService = userService;
    }

    @CheckValidToken
    @HasAnyPermission (permissions = Permission.MANAGE_WATCHER_LIST)
    public Result getAutoWatchRule(final String tenant, final Long ruleId)
    {
        final Option<AutoWatchRule> autoWatchRule = autoWatchRuleService.getAutoWatchRule(tenant, ruleId);
        if (autoWatchRule.isDefined())
        {
            return ok(autoWatchRule.get().toJson());
        }
        else
        {
            return notFound();
        }
    }

    @CheckValidToken
    @HasAnyPermission (permissions = Permission.MANAGE_WATCHER_LIST)
    public Result getAllAutoWatchRules(final String tenant)
    {
        if (!checkValidTenant(tenant))
        {
            return unauthorized("Invalid tenant");
        }

        ArrayNode result = new ArrayNode(JsonNodeFactory.instance);
        result.addAll(Lists.newArrayList(Iterables.transform(autoWatchRuleService.getAllAutoWatchRules(tenant),
                new Function<AutoWatchRule, JsonNode>()
                {
                    @Override
                    public JsonNode apply(AutoWatchRule autoWatchRule)
                    {
                        return autoWatchRule.toJson();
                    }
                }
        )));

        return ok(result);
    }

    @CheckValidToken
    @HasAnyPermission (permissions = Permission.MANAGE_WATCHER_LIST)
    public Result createAutoWatchRule(final String tenant)
    {
        if (!checkValidTenant(tenant))
        {
            return unauthorized("Invalid tenant");
        }
        final JsonNode jsonNode = request().body().asJson();

        final Option<String> user = AC.getUser();

        if (jsonNode.has("projectKey") && user.isDefined())
        {
            final String project = jsonNode.get("projectKey").asText();
            final List<JiraUser> users = userService.getUsers(jsonNode, tenant);
            final String filter = jsonNode.has("filter") ? jsonNode.get("filter").asText() : StringUtils.EMPTY;

            Either<ValidationErrors, AutoWatchRule> autoWatchRuleServiceResult =
                    autoWatchRuleService.createAutoWatchRule(tenant, filter, users, project, userService.getUser(user.get(), tenant));

            return autoWatchRuleServiceResult.fold(
                    new Function<ValidationErrors, Result>()
                    {
                        @Override
                        public Result apply(ValidationErrors validationErrors)
                        {
                            return badRequest(validationErrors.toJson());
                        }
                    },
                    new Function<AutoWatchRule, Result>()
                    {
                        @Override
                        public Result apply(AutoWatchRule autoWatchRule)
                        {
                            return ok(autoWatchRule.toJson());
                        }
                    }
            );
        }
        else
        {
            return badRequest();
        }
    }

    @CheckValidToken
    @HasAnyPermission (permissions = Permission.MANAGE_WATCHER_LIST)
    public Result editAutoWatchRule(final Long ruleId, final String tenant)
    {
        if (!checkValidTenant(tenant))
        {
            return unauthorized("Invalid tenant");
        }
        JsonNode jsonNode = request().body().asJson();

        final Option<AutoWatchRule> autoWatchRule = autoWatchRuleService.getAutoWatchRule(tenant, ruleId);
        final Option<String> user = AC.getUser();
        if (autoWatchRule.isDefined() && user.isDefined())
        {
            final AutoWatchRule rule = autoWatchRule.get();

            if (jsonNode.has("users"))
            {
                rule.users = userService.getUsers(jsonNode, tenant);
            }
            if (jsonNode.has("filter"))
            {
                rule.filter = jsonNode.get("filter").asText();
            }
            rule.ruleOwner = userService.getUser(user.get(), tenant);
            rule.resetExecutions();
            Either<ValidationErrors, AutoWatchRule> autoWatchRuleServiceResult =
                    autoWatchRuleService.updateAutoWatchRule(rule);

            return autoWatchRuleServiceResult.fold(
                    new Function<ValidationErrors, Result>()
                    {
                        @Override
                        public Result apply(ValidationErrors validationErrors)
                        {
                            return badRequest(validationErrors.toJson());
                        }
                    },
                    new Function<AutoWatchRule, Result>()
                    {
                        @Override
                        public Result apply(AutoWatchRule autoWatchRule)
                        {
                            return ok(autoWatchRule.toJson());
                        }
                    }
            );
        }
        else if (autoWatchRule.isEmpty())
        {
            return notFound();
        }
        else if (user.isEmpty())
        {
            return forbidden();
        }
        else
        {
            return badRequest();
        }
    }

    @CheckValidToken
    @HasAnyPermission (permissions = Permission.MANAGE_WATCHER_LIST)
    public Result deleteAutoWatchRule(final String tenant, final Long ruleId)
    {
        if (!checkValidTenant(tenant))
        {
            return unauthorized("Invalid tenant");
        }
        autoWatchRuleService.deleteAutoWatchRule(tenant, ruleId);
        return Results.status(HttpStatus.SC_NO_CONTENT);
    }

    private boolean checkValidTenant(final String tenant)
    {
        if (!AC.getToken().isDefined())
        {
            return false;
        }
        Token token = AC.validateToken(AC.getToken().get(), false).get();

        return token.getAcHost().equals(tenant);
    }
}
