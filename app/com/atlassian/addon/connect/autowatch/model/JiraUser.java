package com.atlassian.addon.connect.autowatch.model;

import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import play.db.jpa.JPA;
import play.libs.F;
import play.libs.Json;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table (name = "jira_user", uniqueConstraints = @UniqueConstraint (columnNames = { "user_name", "tenant" }))
@NamedQueries ({
        @NamedQuery (name = "JiraUser.findByTenantAndUserKeys", query = "SELECT user FROM JiraUser user where user.tenant = :tenant AND user.key IN (:keys)"),
})
public class JiraUser
{
    @Id
    @SequenceGenerator (name = "jira_user_gen", sequenceName = "jira_user_seq")
    @GeneratedValue (generator = "jira_user_gen")
    @Column (name = "jira_user_id")
    public Long id;

    @Column (name = "user_name")
    public String key;

    @Column (nullable = false)
    public String tenant;

    public JiraUser()
    {
    }

    public JiraUser(final String key, final String tenant)
    {
        this.key = key;
        this.tenant = tenant;
    }

    public JsonNode toJson()
    {
        final ObjectNode jsonNode = Json.newObject();
        jsonNode.put("id", id);
        jsonNode.put("key", key);
        return jsonNode;
    }


    public static List<JiraUser> getUsers(final List<String> usersKeys, final String tenant)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<List<JiraUser>>()
            {
                @Override
                public List<JiraUser> apply() throws Throwable
                {
                    return JPA.em().createNamedQuery("JiraUser.findByTenantAndUserKeys", JiraUser.class)
                            .setParameter("tenant", tenant)
                            .setParameter("keys", usersKeys)
                            .getResultList();
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static List<JiraUser> saveUsers(final Iterable<String> userKeys, final String tenant)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<List<JiraUser>>()
            {
                @Override
                public List<JiraUser> apply() throws Throwable
                {
                    return Lists.newArrayList(Iterables.transform(userKeys, new Function<String, JiraUser>()
                    {
                        @Override
                        public JiraUser apply(final String userKey)
                        {
                            final JiraUser jiraUser = new JiraUser(userKey, tenant);
                            JPA.em().persist(jiraUser);
                            return JPA.em().merge(jiraUser);
                        }
                    }));
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static Option<JiraUser> getUser(String userKey, String tenant)
    {
        final List<JiraUser> users = getUsers(Lists.newArrayList(userKey), tenant);
        return Option.option(Iterables.getOnlyElement(users, null));
    }
}
