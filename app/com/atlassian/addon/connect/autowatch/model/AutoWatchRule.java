package com.atlassian.addon.connect.autowatch.model;

import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import play.db.jpa.JPA;
import play.libs.F;
import play.libs.Json;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (name = "auto_watch_rule")
@NamedQueries ({
        @NamedQuery (name = "AutoWatchRule.findByRuleId", query = "SELECT rule FROM AutoWatchRule rule where rule.tenant = :tenant AND rule.id = :id"),
        @NamedQuery (name = "AutoWatchRule.findAllByTenantId", query = "SELECT rule FROM AutoWatchRule rule where rule.tenant = :tenant"),
        @NamedQuery (name = "AutoWatchRule.count", query = "SELECT COUNT (rule) FROM AutoWatchRule rule")
})
public class AutoWatchRule
{
    @Id
    @SequenceGenerator (name = "autowach_rule_gen", sequenceName = "autowach_rule_seq")
    @GeneratedValue (generator = "autowach_rule_gen")
    @Column (name = "autowatch_rule_id")
    public Long id;
    @Column (nullable = false)
    public String tenant;
    @Column (nullable = false)
    public String filter;
    @Column (nullable = false)
    public String project;
    /**
     * Counts number of invalid execution of this rule
     */
    @Column (nullable = false, name = "no_invalid_executions")
    public Integer noInvalidExecutions;
    /**
     * Last message describing why this rule is invalid
     */
    @Column (nullable = true, name = "invalid_execution_message")
    public String invalidExecutionMessage;
    /**
     * Last message describing why this rule is invalid
     */
    @Column (nullable = true, name = "last_executed")
    public Timestamp lastExecuted;
    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable (
            name = "rule_user",
            joinColumns = { @JoinColumn (name = "autowatch_rule_id", referencedColumnName = "autowatch_rule_id") },
            inverseJoinColumns = { @JoinColumn (name = "jira_user_id", referencedColumnName = "jira_user_id") }
    )
    public List<JiraUser> users;
    @ManyToOne (optional = false)
    public JiraUser ruleOwner;

    public AutoWatchRule()
    {
    }

    public AutoWatchRule(String tenant, String filter, String project, List<JiraUser> users, JiraUser ruleOwner)
    {
        this.tenant = tenant;
        this.filter = filter;
        this.project = project;
        this.users = users;
        this.ruleOwner = ruleOwner;
        this.noInvalidExecutions = 0;
        this.invalidExecutionMessage = null;
    }

    public static Option<AutoWatchRule> getRuleForId(final Long id, final String tenant)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<Option<AutoWatchRule>>()
            {
                @Override
                public Option<AutoWatchRule> apply() throws Throwable
                {
                    final List<AutoWatchRule> resultList = JPA.em().createNamedQuery("AutoWatchRule.findByRuleId", AutoWatchRule.class)
                            .setParameter("tenant", tenant)
                            .setParameter("id", id)
                            .getResultList();
                    return resultList.isEmpty() ? Option.<AutoWatchRule>none() : Option.option(resultList.get(0));
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static List<AutoWatchRule> getAllRules(final String tenant)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<List<AutoWatchRule>>()
            {
                @Override
                public List<AutoWatchRule> apply() throws Throwable
                {
                    return JPA.em().createNamedQuery("AutoWatchRule.findAllByTenantId", AutoWatchRule.class)
                            .setParameter("tenant", tenant)
                            .getResultList();
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static AutoWatchRule update(final AutoWatchRule autoWatchRule)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<AutoWatchRule>()
            {
                @Override
                public AutoWatchRule apply() throws Throwable
                {
                    final AutoWatchRule merge = JPA.em().merge(autoWatchRule);
                    JPA.em().persist(merge);
                    JPA.em().flush();
                    JPA.em().refresh(merge);
                    return merge;
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static AutoWatchRule save(final AutoWatchRule autoWatchRule)
    {
        try
        {
            return JPA.withTransaction(new F.Function0<AutoWatchRule>()
            {
                @Override
                public AutoWatchRule apply() throws Throwable
                {
                    JPA.em().persist(autoWatchRule);
                    JPA.em().flush();
                    JPA.em().refresh(autoWatchRule);
                    return autoWatchRule;
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public static void delete(final String tenant, final Long ruleId)
    {
        try
        {
            JPA.withTransaction(new F.Function0<Void>()
            {
                @Override
                public Void apply() throws Throwable
                {
                    final List<AutoWatchRule> resultList = JPA.em().createNamedQuery("AutoWatchRule.findByRuleId", AutoWatchRule.class)
                            .setParameter("tenant", tenant)
                            .setParameter("id", ruleId)
                            .getResultList();
                    final Option<AutoWatchRule> autoWatchRule = resultList.isEmpty() ? Option.<AutoWatchRule>none() : Option.option(resultList.get(0));
                    if (autoWatchRule.isDefined())
                    {
                        JPA.em().remove(autoWatchRule.get());
                    }
                    return null;
                }
            });
        }
        catch (Throwable throwable)
        {
            throw new RuntimeException(throwable);
        }
    }

    public void resetExecutions()
    {
        noInvalidExecutions = 0;
        invalidExecutionMessage = null;
        lastExecuted = null;
    }

    public JsonNode toJson()
    {
        ObjectNode jsonNode = Json.newObject();
        jsonNode.put("id", id);
        jsonNode.put("filter", filter);
        jsonNode.put("projectKey", project);
        jsonNode.put("noInvalidExecutions", noInvalidExecutions);
        jsonNode.put("invalidExecutionMessage", invalidExecutionMessage);
        ArrayNode users = jsonNode.putArray("users");
        users.addAll(Lists.newArrayList(Iterables.transform(this.users, new Function<JiraUser, JsonNode>()
        {
            @Override
            public JsonNode apply(JiraUser jiraUser)
            {
                return jiraUser.toJson();
            }
        })));
        return jsonNode;
    }

}
