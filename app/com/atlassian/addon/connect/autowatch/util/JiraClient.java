package com.atlassian.addon.connect.autowatch.util;

import com.atlassian.connect.play.java.AcHost;
import com.atlassian.fugue.Option;
import play.libs.WS;

public interface JiraClient
{
    WS.WSRequestHolder url(String url, AcHost acHost, Option<String> userId);
}
