define([
    "lib/jquery",
    "lib/restfultable"
],function($, RT) {
    // override of the default implementation as it sets conent type and sends no data
    return RT.EntryModel.extend({
        destroy: function (options) {
            options = options || {};

            var instance = this,
                url = this.url();

            $.ajax({
                url: url,
                type: "DELETE",
                dataType: "json",
                success: function (data) {
                    if(instance.collection){
                        instance.collection.remove(instance);
                    }
                    if (options.success) {
                        options.success.call(instance, data);
                    }
                },
                error: function (xhr) {
                    instance._serverErrorHandler(xhr, this);
                    if (options.error) {
                        options.error.call(instance, xhr);
                    }
                }
            });

            return this;
        }
    });
});