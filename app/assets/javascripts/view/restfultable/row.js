define([
    "lib/jquery",
    "lib/underscore",
    "lib/backbone",
    "lib/connect",
    "lib/restfultable",
    "util/util"
],function($, _, Backbone, AP, RT, util) {
    return RT.Row.extend({
        render: function() {
            var el = RT.Row.prototype.render.apply(this, Array.prototype.slice.call(arguments));
            if (this.model.get("noInvalidExecutions") > 0) {
                this.$el.addClass("invalid-rule");
            } else {
                this.$el.removeClass("invalid-rule");
            }
            return el;
        }
    });
});