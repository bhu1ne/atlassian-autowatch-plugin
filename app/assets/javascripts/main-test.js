//Main file for loading RequireJS necessary bits
QUnit.config.autostart = false;

require([
    'util/cachedRequest-test',
    'util/util-test'
], function(){
    QUnit.start(); //Tests loaded, run tests
});